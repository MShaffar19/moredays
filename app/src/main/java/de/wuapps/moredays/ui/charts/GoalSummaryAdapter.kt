package de.wuapps.moredays.ui.charts

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.core.text.HtmlCompat
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import de.wuapps.moredays.MoreDaysApplication
import de.wuapps.moredays.R
import de.wuapps.moredays.databinding.ListItemGoalSummaryBinding
import de.wuapps.moredays.ui.customviews.CircleDrawable

class GoalSummaryAdapter :
    ListAdapter<GoalsChartsViewModel.GoalSummary, GoalSummaryAdapter.GoalSummaryViewHolder>(
        GoalSummaryDiffCallback
    ) {
    class GoalSummaryViewHolder(private val binding: ListItemGoalSummaryBinding) :
        RecyclerView.ViewHolder(binding.root) {

        fun bind(item: GoalsChartsViewModel.GoalSummary) {
            binding.imageViewIcon.setImageDrawable(
                MoreDaysApplication.iconPack?.getIcon(item.goal.symbol)?.drawable
            )
            binding.imageViewIcon.setColorFilter(item.goal.color)
            binding.lblMade.text = HtmlCompat.fromHtml(
                String.format(
                    "&#127941; %d %s",
                    item.countDaysMade,
                    itemView.context.getString(R.string.trophy_type_title_made_goal)
                ), HtmlCompat.FROM_HTML_MODE_LEGACY
            )
            binding.lblPoints.text = HtmlCompat.fromHtml(
                String.format(
                    "&sum; %d %s",
                    item.sumPoints,
                    itemView.context.getString(R.string.lbl_points)
                ), HtmlCompat.FROM_HTML_MODE_LEGACY
            )
            binding.lblInfo.text = HtmlCompat.fromHtml(
                String.format(
                    "&#9939; %d %s",
                    item.countDaysInRow,
                    itemView.context.getString(R.string.trophy_type_title_row)
                ), HtmlCompat.FROM_HTML_MODE_LEGACY
            )
            val percent: Float =
                item.sumPoints.toFloat() / (item.daysTotal * item.goal.points) * 100.0f
            binding.layout.background = CircleDrawable(
                item.goal.color,
                percent,
                50
            )


        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): GoalSummaryViewHolder {
        return GoalSummaryViewHolder(
            ListItemGoalSummaryBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            )
        )
    }

    override fun onBindViewHolder(holder: GoalSummaryViewHolder, position: Int) {
        val goalSummary = getItem(position)
        if (goalSummary != null)
            holder.bind(goalSummary)
    }
}
object GoalSummaryDiffCallback : DiffUtil.ItemCallback<GoalsChartsViewModel.GoalSummary>() {
    override fun areItemsTheSame(
        oldItem: GoalsChartsViewModel.GoalSummary,
        newItem: GoalsChartsViewModel.GoalSummary
    ): Boolean {
        return oldItem.goal.uid == newItem.goal.uid
    }

    override fun areContentsTheSame(
        oldItem: GoalsChartsViewModel.GoalSummary,
        newItem: GoalsChartsViewModel.GoalSummary
    ): Boolean {
        return oldItem == newItem
    }
}
