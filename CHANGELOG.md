# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.1.0/).

<!--
################################################################################
### PLEASE LINK THE ISSUES IF THERE IS ONE, OTHERWISE LINK THE PULL-REQUESTS ###
################################################################################
e.g. - - Add new unit type `Assignment`. [#600](https://gitlab.com/wuapps/moredays/-/issues/600)
-->

## [Unreleased]

## [1.0.7] - 2022-04-14 - 🏁-Release

### Fixed

- 100% line after editing goal points or activate/inactivate a goal
- update libs and fix code inspection issues

### Added

- support negative points in activities

## [1.0.6] - 2021-12-15 - 🏁-Release

### Fixed

- fix/refactor/style: lottie, dark mode fixes, hundred percent fixes
